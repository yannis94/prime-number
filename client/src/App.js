import { useState } from 'react'
import Answer from './components/Answer'
import logo from './logo.svg';
import './App.css';

function App() {
    const [num, setNum] = useState(null)
    const [showAnswer, setShowAnswer] = useState(false)
    const [answer, setAnswer] = useState(null)

    function handleChange(e) {
        const prov = e.target.value
        setShowAnswer(false)
        setNum(prov)
    }

    function handleClick() {
        console.log(num)
        fetch(`http://localhost:3001/api/prime-number/${num}`, {
            header: {
                'Access-Control-Allow-Origin': '*'
            }
        }) 
        .then(response => {
            console.log(response)
            return response.json()
        })
        .then(decoded => {
            console.log(decoded)
            setShowAnswer(true)
            setAnswer(decoded)
        })
        .catch(err => console.log(err))
    }
    return (
        <div className="App">
            <header className="App-header">
                <img src={logo} className="App-logo" alt="logo" />
                <h1>
                    Is it a <strong>PRIME NUMBER</strong>
                </h1>
                <input id="userInput" type="number" step="1" min="0" placeholder="Enter a number" onChange={handleChange} />
                <button onClick={handleClick}>Send</button>
                { showAnswer ? <Answer result={answer.result} number={answer.number} /> : null }
            </header>
        </div>
    );
}

export default App;
