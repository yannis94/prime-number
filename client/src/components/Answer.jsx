export default function Answer(props) {
    return (
        <div>
            <h2>{props.number}</h2>
            <p>{props.result}</p>
        </div>
    )
}
