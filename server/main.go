package main

import (
    "fmt"
    "github.com/gorilla/mux"
    "net/http"
    "strconv"
    "encoding/json"
    prime "server/prime"
)

const Host string = "http://localhost:8080"

type Resp struct {
    Number string `json:"number"`
    Result string `json:"result"`
}

func sanitize(input string) int64 {
    n, err := strconv.ParseInt(input, 10, 64)

    fmt.Printf("Looking for %v...\n", n)
    
    if err != nil {
        return -404
    }
    return n
}

func userAskNum(w http.ResponseWriter, r *http.Request) {
    fmt.Printf("%v %v\n", r.Method, r.URL)

    param := mux.Vars(r)
    var serverResponse Resp
    userInput := param["num"]

    serverResponse.Number = userInput

    userNumber := sanitize(userInput)

    w.Header().Set("Content-Type", "application/json")
    w.Header().Set("Access-Control-Allow-Origin", "*")

    if userNumber == -404 {
        serverResponse.Result = "You should ask for a positive number"
    } else {
        result := prime.IsPrimeNumber(userNumber) 

        if result {
            serverResponse.Result = "Waow, this is a prime number"
        } else {
            serverResponse.Result = "This is not a prime number"
        }
    }

    fmt.Printf("%v", serverResponse.Result)

    w.WriteHeader(200)
    json.NewEncoder(w).Encode(serverResponse)

}

func main() {
    r := mux.NewRouter()

    r.HandleFunc("/api/prime-number/{num}", userAskNum)


    http.ListenAndServe(":8080", r)
}
