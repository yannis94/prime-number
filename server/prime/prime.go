package prime

func IsPrimeNumber(num int64) bool {
    var isPrime bool = true

    if num < 2 {
        isPrime = false
    } else {
        var i int64 = 2

        for i < num {
            if num % i == 0 {
                isPrime = false
                break
            }
            i++
        }
    }

    return isPrime
}

