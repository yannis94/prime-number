package prime

import (
    "testing"
)

func FuzzIsPrimeNumber(f *testing.F) {

    testcases := []int{3, 5, 6, 7, 1997}

    for _, tc := range testcases {
        f.Add(tc)
    }

    f.Fuzz(func(t *testing.T, orig int) {
        n := int64(orig)
        isPrime := IsPrimeNumber(n)
        test := true

        if n < 2 {
            test = false
        }

        var i int64 = 2

        for i < n {
            if n%i == 0 {
                test = false
            }
            i++
        }

        if isPrime != test {
            var err string
            if test == true {
                err = "should be"
            } else {
                err = "shouldn't be"
            }

            t.Errorf("%d %s a prime number", n, err)

        }
    })
}
